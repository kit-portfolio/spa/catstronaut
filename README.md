### Project structure

- `server`: The starting point of GraphQL server.
- `client`: The starting point of React application.

### To get started:
Start the GraphQL API server.
1. Navigate to the `server` folder.
1. Run `npm install`.
1. Run `npm start`.

In another terminal window, start frontend app.
1. Navigate to the `client` folder.
1. Run `npm install`.
1. Run `npm start`.
