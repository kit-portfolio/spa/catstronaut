import React from 'react';
import ReactDOM from 'react-dom';
import {GlobalStyles} from "./styles/global-styles";
import {PageController} from "@pages/controller";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";

const client = new ApolloClient({
    uri: "http://localhost:4000",
    cache: new InMemoryCache(),
});

ReactDOM.render(
    <ApolloProvider client={client}>
        <GlobalStyles />
        <PageController />
    </ApolloProvider>,
    document.getElementById("root")
);
