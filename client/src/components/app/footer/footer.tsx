import React from 'react';
import { ApolloIcon} from "@assets/apollo-icons";
import {FooterContainer, LogoContainer} from "@components/app/footer/footer.styles";

/**
 * Footer is useless component to make our app look a little closer to a real website!
 */
export const Footer: React.FC = () => {
  return (
    <FooterContainer>
      2021 ©{' '}
      <LogoContainer>
        <ApolloIcon width="100px" height="40px" />
      </LogoContainer>
    </FooterContainer>
  );
};

