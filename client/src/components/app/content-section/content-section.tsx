import React from 'react';
import {ContentDiv} from "@components/app/content-section/content-section.style";

/**
 * Content Section component renders content (mainly text/mdown based)
 * for course detail and lesson detail
 */
export const ContentSection: React.FC<React.PropsWithChildren> = ({ children })=> {
  return <ContentDiv>{children}</ContentDiv>;
};
