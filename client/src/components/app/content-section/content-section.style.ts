import styled from "@emotion/styled";
import {colors} from "@styles/palette";
import {widths} from "@styles/layout.styles";

export const ContentDiv = styled.div({
    marginTop: 10,
    display: 'flex',
    flexDirection: 'column',
    maxWidth: widths.textPageWidth,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: colors.background,
});
