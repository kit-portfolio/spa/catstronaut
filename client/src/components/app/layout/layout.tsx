import React, {PropsWithChildren} from 'react';
import {Header} from "@components/app/header/header";
import {Footer} from "@components/app/footer/footer";
import {PageContainer} from "@components/app/layout/layout.styles";

export interface LayoutProps {
    fullWidth?: boolean;
    grid?: boolean;
}

/**
 * Layout renders the full page content:
 * with header, Page container and footer
 */
export const Layout: React.FC<PropsWithChildren<LayoutProps>> = ({fullWidth, children, grid}) => {
    return (
        <>
            <Header/>
            <PageContainer fullWidth={fullWidth} grid={grid}>
                {children}
            </PageContainer>
            <Footer/>
        </>
    );
};
