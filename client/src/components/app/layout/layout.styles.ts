import styled from "@emotion/styled";
import {unit} from "@styles/layout.styles";
import {widths} from "@styles/layout.styles";
import {LayoutProps} from "@components/app/layout/layout";

/** Layout styled components */
export const PageContainer = styled.div<LayoutProps>`
  display: flex;
  justify-content: ${({grid}) => grid ? 'center' : 'top'};
  flex-direction: ${({grid}) => grid ? 'row' : 'column'};
  flex-wrap: wrap;
  align-self: center;
  flex-grow: 1;
  max-width: ${({fullWidth}) => fullWidth ? undefined : `${widths.regularPageWidth}px`};
  width: '100%';
  padding: ${({fullWidth}) => fullWidth ? 0 : unit * 2};
  padding-bottom: unit * 5;
`;
