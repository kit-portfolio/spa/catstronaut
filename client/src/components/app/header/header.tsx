import React, {PropsWithChildren} from 'react';
import logo from '@assets/space_cat_logo.png';
import {route} from "@constants/route.constants";
import {
    Container,
    HeaderBar,
    HomeButton,
    HomeButtonContainer,
    HomeLink,
    Logo,
    Title
} from "@components/app/header/header.styles";
import {LogoContainer} from "@components/app/footer/footer.styles";

/**
 * Header renders the top navigation
 * for this particular tutorial level, it only holds the home button
 */
export const Header: React.FC<PropsWithChildren> = ({children}) => {
    return (
        <HeaderBar>
            <Container>
                <HomeButtonContainer>
                    <HomeLink to={route.home()}>
                        <HomeButton>
                            <LogoContainer>
                                <Logo src={logo}/>
                            </LogoContainer>
                            <Title>
                                <h3>Catstronaut</h3>
                                <div>Kitty space academy</div>
                            </Title>
                        </HomeButton>
                    </HomeLink>
                </HomeButtonContainer>
                {children}
            </Container>
        </HeaderBar>
    );
};
