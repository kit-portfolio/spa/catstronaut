import styled from "@emotion/styled";
import {Link} from "react-router-dom";
import {colors} from "@styles/palette";
import {widths} from "@styles/layout.styles";

/** Header styled components */
export const HeaderBar = styled.div({
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottom: `solid 1px ${colors.pink.light}`,
    boxShadow: '0px 1px 5px 0px rgba(0,0,0,0.15)',
    padding: '5px 30px',
    minHeight: 80,
    backgroundColor: 'white',
});

export const Container = styled.div({
    width: `${widths.regularPageWidth}px`,
});

export const HomeLink = styled(Link)({
    textDecoration: 'none',
});

export const HomeButtonContainer = styled.div({
    display: 'flex',
    flex: 1,
});

export const HomeButton = styled.div({
    display: 'flex',
    flexDirection: 'row',
    color: colors.accent,
    alignItems: 'center',
    ':hover': {
        color: colors.pink.dark,
    },
});

export const LogoContainer = styled.div({ display: 'flex', alignSelf: 'center' });

export const Logo = styled.img({
    height: 60,
    width: 60,
    marginRight: 8,
});

export const Title = styled.div({
    display: 'flex',
    flexDirection: 'column',
    h3: {
        lineHeight: '1em',
        marginBottom: 0,
    },
    div: {
        fontSize: '0.9em',
        lineHeight: '0.8em',
        paddingLeft: 2,
    },
});
