import React from 'react';
import {IconArrowRight, IconDoubleArrowRight} from "@assets//apollo-icons";
import {humanReadableTimeFromSeconds} from "@utils/helpers";
import {route} from "@constants/route.constants";
import {
    ModuleListItem,
    ModuleListItemContent,
    ModuleNavStyledLink,
    ModulesList,
    ModulesNavContainer
} from "@components/generic/modules-navigation/modules-navigation.styles";
import {ModuleTitle} from "@components/generic/module-detail/module-detail-styles";

/**
 * Module Navigation: displays a list of modules titles
 * from a track and navigates to the modules page
 */
export const ModulesNav: React.FC<{ module: any, track: any }> = ({module, track}) => {
    return (
        <ModulesNavContainer>
            <ModuleTitle>
                <h4>{track.title}</h4>
            </ModuleTitle>
            <ModulesList>
                {track.modules.map((navModule: any) => (
                    <ModuleListItem key={`module_${navModule.id}`}>
                        <div>
                            <ModuleNavStyledLink to={route.module(track.id, navModule.id)}>
                                <ModuleListItemContent isActive={navModule.id === module.id}>
                                    {navModule.id === module.id ? (
                                        <IconDoubleArrowRight width="14px"/>
                                    ) : (
                                        <IconArrowRight width="14px" weight="thin"/>
                                    )}
                                    <div>{navModule.title}</div>
                                    <div>{humanReadableTimeFromSeconds(navModule.length)}</div>
                                </ModuleListItemContent>
                            </ModuleNavStyledLink>
                        </div>
                    </ModuleListItem>
                ))}
            </ModulesList>
        </ModulesNavContainer>
    );
};
