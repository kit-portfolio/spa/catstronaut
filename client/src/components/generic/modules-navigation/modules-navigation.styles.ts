import styled from "@emotion/styled";
import {Link} from "react-router-dom";
import {colors} from "@styles/palette";

export const ModulesNavContainer = styled.div({
    width: '33%',
    position: 'relative',
    marginLeft: 20,
    backgroundColor: colors.black.light,
    borderRadius: 4,
    border: `solid 1px ${colors.black.lighter}`,
    overflow: 'auto',
});

export const trackTitleHeight = 70;

export const ModuleTitle = styled.div({
    display: 'flex',
    position: 'sticky',
    fontSize: '1.6em',
    fontWeight: '400',
    height: trackTitleHeight,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: 'colors.pink.base',
    borderBottom: `solid 1px ${colors.pink.base}`,

    a: {
        textDecoration: 'none',
        color: colors.silver.base,
    },
    ':hover': {
        backgroundColor: colors.black.base,
    },
});

export const ModulesList = styled.ul({
    listStyle: 'none',
    margin: 0,
    padding: 0,
    overflowY: 'scroll',
    height: `calc(100% - ${trackTitleHeight}px)`,
});

export const ModuleListItem = styled.li((props) => ({
    borderBottom: `solid 1px ${colors.grey.darker}`,
    ':last-child': {
        borderBottom: 'none',
    },
}));

export const ModuleNavStyledLink = styled(Link)({
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
});

export const ModuleListItemContent = styled.div((props: { isActive: boolean }) => ({
    backgroundColor: props.isActive ? colors.black.base : colors.black.light,
    color: props.isActive ? colors.silver.lighter : colors.silver.darker,
    minHeight: 80,
    padding: '10px 20px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontSize: '1.1em',
    flex: 1,
    ':hover': {
        backgroundColor: props.isActive ? colors.black.dark : colors.black.base,
        color: 'white',
    },
}));
