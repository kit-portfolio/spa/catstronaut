import React from 'react';
import useWindowDimensions from "@utils/useWindowDimensions";
import {ContentSection} from "@components/app/content-section/content-section";
import ReactPlayer from "react-player";
import {ModulesNav} from "@components/generic/modules-navigation/modules-navigation";
import {MarkDown} from "@components/utils/md-content/md-content";
import {
    ModuleTitle,
    PlayerContainer,
    TopContainer,
    TopSection
} from "@components/generic/module-detail/module-detail-styles";

/**
 * Module Detail renders content of a given module:
 * Video player, modules navigation and markdown content
 */
export const ModuleDetail: React.FC<{ track: any, module: any }> = ({track, module}) => {
    const {videoUrl, title, content} = module;
    const {width} = useWindowDimensions();

    return (
        <>
            <TopSection>
                <TopContainer totalWidth={width}>
                    <PlayerContainer>
                        <ReactPlayer url={videoUrl} width="100%" height="100%"/>
                    </PlayerContainer>
                    <ModulesNav track={track} module={module}></ModulesNav>
                </TopContainer>
            </TopSection>
            <ContentSection>
                <ModuleTitle>{title}</ModuleTitle>
                <MarkDown content={content}/>
            </ContentSection>
        </>
    );
};
