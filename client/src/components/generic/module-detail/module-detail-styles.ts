import styled from "@emotion/styled";
import {colors} from "@styles/palette";
import {widths} from "@styles/layout.styles";

export const TopSection = styled.div({
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: colors.black.base,
    padding: 20,
    borderBottom: `solid 1px ${colors.pink.base}`,
});

export const TopContainer = styled.div(({totalWidth}: { totalWidth: number }) => ({
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'center',
    width: '100%',
    maxWidth: widths.largePageWidth,
    // 60 below removes 3 * 20 horizontal paddings (sides and inner between player and list)
    height: ((totalWidth - 60) * (2 / 3)) / (16 / 9),
    maxHeight: (widths.largePageWidth * (2 / 3)) / (16 / 9),
}));

export const PlayerContainer = styled.div({
    width: '66%',
});

export const ModuleTitle = styled.h1({
    marginTop: 10,
    marginBottom: 30,
    paddingBottom: 10,
    color: colors.black.lighter,
    borderBottom: `solid 1px ${colors.pink.base}`,
});
