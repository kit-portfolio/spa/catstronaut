import React from "react";
import {humanReadableTimeFromSeconds} from "@utils/helpers";
import type {Track} from '@api/graphql'
import {useMutation} from "@apollo/client";
import {route} from "@constants/route.constants";
import {
    AuthorAndTrack,
    AuthorImage,
    AuthorName,
    CardBody,
    CardContainer,
    CardContent,
    CardFooter,
    CardImage,
    CardImageContainer,
    CardTitle,
    TrackLength
} from "@components/generic/tack-card/tack-card.styles";
import {INCREMENT_TRACK_VIEWS} from "@components/generic/tack-card/track-card.api";

/**
 * Track Card component renders basic info in a card format
 * for each track populating the tracks grid homepage.
 */
export const TrackCard: React.FC<{ track: Track }> = ({track}) => {
    const {title, thumbnail, author, length, modulesCount, id} = track;
    const [incrementTrackViews] = useMutation(INCREMENT_TRACK_VIEWS, {
        variables: { incrementTrackViewsId: id },
        // to observe what the mutation response returns
        onCompleted: (data) => {
            console.log(data);
        },
    });

    return (
        <CardContainer
            to={route.track(id)}
            onClick={() => incrementTrackViews()}
        >
            <CardContent>
                <CardImageContainer>
                    <CardImage src={thumbnail || ""} alt={title}/>
                </CardImageContainer>
                <CardBody>
                    <CardTitle>{title || ""}</CardTitle>
                    <CardFooter>
                        <AuthorImage src={author.photo || ""}/>
                        <AuthorAndTrack>
                            <AuthorName>{author.name}</AuthorName>
                            <TrackLength>
                                {modulesCount} modules -{" "}
                                {humanReadableTimeFromSeconds(length || 0)}
                            </TrackLength>
                        </AuthorAndTrack>
                    </CardFooter>
                </CardBody>
            </CardContent>
        </CardContainer>
    );
};
