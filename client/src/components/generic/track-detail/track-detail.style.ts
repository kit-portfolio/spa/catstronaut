import styled from "@emotion/styled";
import {Link} from "react-router-dom";
import {colors} from "@styles/palette";

export const CoverImage = styled.img({
    objectFit: 'cover',
    maxHeight: 400,
    borderRadius: 4,
    marginBottom: 30,
});

export const StyledLink = styled(Link)({
    textDecoration: 'none',
    color: 'white',
});

export const TrackDetails = styled.div({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 20,
    borderRadius: 4,
    marginBottom: 30,
    border: `solid 1px ${colors.silver.dark}`,
    backgroundColor: colors.silver.lighter,
    h1: {
        width: '100%',
        textAlign: 'center',
        marginBottom: 5,
    },
    h4: {
        fontSize: '1.2em',
        marginBottom: 5,
        color: colors.text,
    },
});

export const DetailRow = styled.div({
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingBottom: 20,
    marginBottom: 20,
    borderBottom: `solid 1px ${colors.silver.dark}`,
});

export const DetailItem = styled.div({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    color: colors.textSecondary,
    alignSelf: 'center',
});

export const AuthorImage = styled.img({
    height: 30,
    width: 30,
    marginBottom: 8,
    borderRadius: '50%',
    objectFit: 'cover',
});

export const AuthorName = styled.div({
    lineHeight: '1em',
    fontSize: '1em',
});

export const IconAndLabel = styled.div({
    display: 'flex',
    flex: 'row',
    alignItems: 'center',
    maxHeight: 20,
    width: '100%',
    div: {
        marginLeft: 8,
    },
    svg: {
        maxHeight: 16,
    },
    '#viewCount': {
        color: colors.pink.base,
    },
});

export const ModuleListContainer = styled.div({
    width: '100%',
    ul: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        marginTop: 5,
        li: {
            fontSize: '1em',
            display: 'flex',
            justifyContent: 'space-between',
            paddingBottom: 2,
        },
    },
});

export const ModuleLength = styled.div({
    marginLeft: 30,
    color: colors.grey.light,
});
