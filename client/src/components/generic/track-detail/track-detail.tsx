import React from 'react';
import {
    Button,
    IconRun,
    IconView,
    IconTime,
    IconBook
} from "@assets/apollo-icons";
import {humanReadableTimeFromSeconds} from "@utils/helpers";
import {ContentSection} from "@components/app/content-section/content-section";
import {MarkDown} from "@components/utils/md-content/md-content";
import {route} from "@constants/route.constants";
import {
    AuthorImage, AuthorName,
    CoverImage,
    DetailItem,
    IconAndLabel,
    ModuleLength,
    ModuleListContainer,
    StyledLink,
    TrackDetails,
    DetailRow
} from "@components/generic/track-detail/track-detail.style";
import {colors} from "@styles/palette";

/**
 * Track Detail component renders the main content of a given track:
 * author, length, number of views, modules list, among other things.
 * It provides access to the first module of the track.
 */
export const TrackDetail: React.FC<{ track: any }> = ({track}) => {
    const {
        title,
        description,
        thumbnail,
        author = {photo: '', name: ''},
        length,
        modulesCount,
        modules = [],
        numberOfViews,
    } = track ?? {};

    return (
        <ContentSection>
            <CoverImage src={thumbnail ?? ''} alt=""/>
            <TrackDetails>
                <DetailRow>
                    <h1>{title}</h1>
                </DetailRow>
                <DetailRow>
                    <DetailItem>
                        <h4>Track details</h4>
                        <IconAndLabel>
                            <IconView width="16px"/>
                            <div id="viewCount">{numberOfViews} view(s)</div>
                        </IconAndLabel>
                        <IconAndLabel>
                            <IconBook width="14px" height="14px"/>
                            <div>{modulesCount} modules</div>
                        </IconAndLabel>
                        <IconAndLabel>
                            <IconTime width="14px"/>
                            <div>{humanReadableTimeFromSeconds(length ?? 0)}</div>
                        </IconAndLabel>
                    </DetailItem>
                    <DetailItem>
                        <h4>Author</h4>
                        <AuthorImage src={author.photo ?? ''}/>
                        <AuthorName>{author.name}</AuthorName>
                    </DetailItem>
                    <div>
                        <StyledLink to={route.module(track.id, modules[0]['id'])}>
                            <Button
                                icon={<IconRun width="20px"/>}
                                color={colors.pink.base}
                                size="large"
                            >
                                Start Track
                            </Button>
                        </StyledLink>
                    </div>
                </DetailRow>
                <ModuleListContainer>
                    <DetailItem>
                        <h4>Modules</h4>
                        <ul>
                            {modules.map((module: any) => (
                                <li key={module.title}>
                                    <div>{module.title}</div>
                                    <ModuleLength>
                                        {humanReadableTimeFromSeconds(module.length ?? 0)}
                                    </ModuleLength>
                                </li>
                            ))}
                        </ul>
                    </DetailItem>
                </ModuleListContainer>
            </TrackDetails>
            <MarkDown content={description}/>
        </ContentSection>
    );
};
