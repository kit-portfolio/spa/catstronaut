import React from 'react';
import {StyledMarkdown} from "@components/utils/md-content/md-content.styles";

/**
 * Markdown component is a simple style wrapper for markdown content used across our app
 */
export const MarkDown: React.FC<{ content?: string | null }> = ({ content }) => {
  return <StyledMarkdown children={content} />;
};
