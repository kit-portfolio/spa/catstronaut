import React from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import {route} from "@constants/route.constants";
/** importing our pages */
import {HomePage} from "@pages/home/home.page";
import {TrackPage} from "@pages/track/track.page";
import {ModulePage} from "@pages/module/module.page";

export const PageController = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route element={<HomePage/>} path={route.home()}/>
                <Route element={<TrackPage/>} path={route.track(':trackId')}/>
                <Route element={<ModulePage/>} path={route.module(":trackId", ":moduleId")}/>
            </Routes>
        </BrowserRouter>
    );
}
