import {gql} from "@api/gql";

export const GET_HOME_PAGE_DATA = gql(`
 query GetTracks {
  tracksForHome {
    id
    title
    thumbnail
    length
    modulesCount
    modules {
      id
      content
      length
      title
      videoUrl
    }
    author {
      id
      name
      photo
    }
  }
}
`);
