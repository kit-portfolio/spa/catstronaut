import React from "react";
import { Layout} from "@components/app/layout/layout";
import { useQuery } from "@apollo/client";
import {TrackCard} from "@components/generic/tack-card/track-card";
import {QueryResult} from "@components/utils/query-result/query-result";
import {GET_HOME_PAGE_DATA} from "@pages/home/home-page.api";

/**
 * Tracks Page is the Catstronauts home page.
 * We display a grid of tracks fetched with useQuery with the TRACKS query
 */
const HomePage = () => {
  const { loading, error, data } = useQuery(GET_HOME_PAGE_DATA);

  return (
      <Layout grid>
        <QueryResult error={error} loading={loading} data={data}>
          {data?.tracksForHome?.map((track) => (
              <TrackCard key={track.id} track={track} />
          ))}
        </QueryResult>
      </Layout>
  );
};

export {HomePage};
