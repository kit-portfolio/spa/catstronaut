import React from "react";
import {useQuery} from "@apollo/client";
import {QueryResult} from "@components/utils/query-result/query-result";
import {useParams} from "react-router-dom";
import {TrackDetail} from "@components/generic/track-detail/track-detail";
import {GET_TRACK_PAGE_DATA} from "./track-page.api";
import {Layout} from "@components/app/layout/layout";

const TrackPage = () => {
    const {trackId = ""} = useParams();
    const {loading, error, data} = useQuery(GET_TRACK_PAGE_DATA, {
        variables: {trackId},
    });
    return <Layout>
        <QueryResult error={error} loading={loading} data={data}>
            <TrackDetail track={data?.track}/>
        </QueryResult>
    </Layout>;
};

export {TrackPage};
