import React from "react";
import {useQuery} from "@apollo/client";
import {QueryResult} from "@components/utils/query-result/query-result";
import {Layout} from "@components/app/layout/layout";
import {useParams} from "react-router-dom";
import {ModuleDetail} from "@components/generic/module-detail/module-detail";
import {GET_MODULE_PAGE_DATA} from "@pages/module/module-page.api";

/**
 * Module page fetches both parent track and module's data from the gql query GET_MODULE_AND_PARENT_TRACK
 * and feeds them to the ModuleDetail component
 */
const ModulePage = () => {
    const {trackId = "", moduleId = ""} = useParams();
    const {loading, error, data} = useQuery(GET_MODULE_PAGE_DATA, {
        variables: {trackId, moduleId},
    });

    return <Layout fullWidth>
        <QueryResult error={error} loading={loading} data={data}>
            <ModuleDetail module={data?.module} track={data?.track}/>
        </QueryResult>
    </Layout>;
};

export {ModulePage};
