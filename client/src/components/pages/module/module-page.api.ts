import {gql} from "@api/gql";

export const GET_MODULE_PAGE_DATA = gql(`
  query getModulePageData($moduleId: ID!, $trackId: ID!) {
  module(id: $moduleId) {
    id
    title
    videoUrl
    content
  }
  track(id: $trackId) {
    id
    title
    modules {
      id
      title
      length
    }
  }
}
`);
