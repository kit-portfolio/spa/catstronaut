export const route = {
    home: () => '/',

    module: (trackId: string, moduleId: string) => `/track/${trackId}/module/${moduleId}`,

    track: (trackId: string) => `/track/${trackId}`,

    placeholder: () => '#',
}
