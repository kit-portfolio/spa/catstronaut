import {colors as SKColors} from "@apollo/space-kit/colors/colors";

export const colors = {
    primary: SKColors.indigo.base,
    secondary: SKColors.teal.base,
    accent: SKColors.pink.base,
    background: SKColors.silver.light,
    text: SKColors.black.base,
    textSecondary: SKColors.grey.dark,
    ...SKColors,
};
