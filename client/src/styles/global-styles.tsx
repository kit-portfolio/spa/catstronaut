import {Global} from "@emotion/core";
import React from "react";
import {colors} from "@styles/palette";

export const GlobalStyles = () => (
    <Global
        styles={{
            html: {
                height: '100%',
            },
            body: {
                margin: 0,
                height: '100%',
                padding: 0,
                fontFamily: "'Source Sans Pro', sans-serif",
                backgroundColor: colors.background,
                color: colors.text,
            },
            '#root': {
                display: 'flex',
                flexDirection: 'column',
                minHeight: '100%',
                backgroundImage: 'url("/space_kitty_pattern.png")',
            },
            '*': {
                boxSizing: 'border-box',
            },
            [['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].join(',')]: {
                margin: 0,
                fontWeight: 600,
            },
            h1: {
                fontSize: 40,
                lineHeight: 1,
            },
            h2: {
                fontSize: 36,
            },
            h3: {
                fontSize: 30,
            },
            h5: {
                fontSize: 16,
                textTransform: 'uppercase',
                letterSpacing: 4,
            },
        }}
    />
);
