import { defineConfig } from 'vite'
import fs from 'fs/promises';
import { resolve } from 'path'

/*
  This override allows us to use .js files instead of exclusively .jsx.
  We should remove as soon as video updates can be prioritized.
*/
export default defineConfig(() => ({
  test: {
    globals: true,
    environment: 'happy-dom',
  },
  resolve: {
    alias: [
        { find: "@assets", replacement: resolve(__dirname, "src/assets") },
        { find: "@api", replacement: resolve(__dirname, "src/types/__generated__") },
        { find: "@pages", replacement: resolve(__dirname, "src/components/pages") },
        { find: "@constants", replacement: resolve(__dirname, "src/constants") },
        { find: "@components", replacement: resolve(__dirname, "src/components") },
        { find: "@utils", replacement: resolve(__dirname, "src/utils") },
        { find: "@styles", replacement: resolve(__dirname, "src/styles") },
    ]
  },
  server: {
    host: 'localhost',
    port: 3000
  },
  esbuild: {
    loader: "tsx",
    include: /src\/.*\.[tj]sx?$/,
    exclude: [],
  },
  optimizeDeps: {
    esbuildOptions: {
      loader: {
        '.js': 'jsx',
      },
      plugins: [
        {
          name: "load-js-files-as-jsx",
          setup(build) {
            build.onLoad({ filter: /src\/.*\.js$/ }, async (args) => ({
              loader: "jsx",
              contents: await fs.readFile(args.path, "utf8"),
            }));
          },
        },
      ],
    },
  },
}));
