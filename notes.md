# LIFT off
schema-first approach. implementing app basind oon exact data app needs.
schema-first designs steps:
1. Defining the schema: We identify which data our feature requires, and then we structure our schema to provide that data as intuitively as possible.
2. Backend implementation: We build out our GraphQL API using Apollo Server and fetch the required data from whichever data sources contain it. In this first course, we will be using mocked data. In a following course, we'll connect our app to a live REST data source.
3. Frontend implementation: Our client consumes data from our GraphQL API to render its views.

One of the benefits of schema-first design is that it reduces total development time by allowing frontend and backend teams to work in parallel. The frontend team can start working with mocked data as soon as the schema is defined, while the backend team develops the API based on that same schema. 

Graph
It is a collection of nodes and edges representing our app data.
we think of each object as a node and each relationship as an edge between two nodes


# Schema Definition Language,


# Lift off II
What exactly is a resolver? A resolver is a function. It has the same name as the field that it populates data for. It can fetch data from any data source, then transforms that data into the shape your client requires.
Resolver functions have a specific signature with four optional parameters:
Let's go over each parameter briefly to understand what they're responsible for:

parent:
parent is the returned value of the resolver for this field's parent. This will be useful when dealing with resolver chains.
args:
args is an object that contains all GraphQL arguments that were provided for the field by the GraphQL operation. When querying for a specific item (such as a specific track instead of all tracks), in client-land we'll make a query with an id argument that will be accessible via this args parameter in server-land. We'll cover this further in Lift-off III.
contextValue:
contextValue is an object shared across all resolvers that are executing for a particular operation. The resolver needs this argument to share state, like authentication information, a database connection, or in our case the RESTDataSource.
info:
info contains information about the operation's execution state, including the field name, the path to the field from the root, and more. It's not used as frequently as the others, but it can be useful for more advanced actions like setting cache policies at the resolver level.
